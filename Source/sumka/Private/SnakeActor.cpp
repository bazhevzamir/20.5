


#include "SnakeActor.h"
#include "SnakeElementBase.h"

ASnakeActor::ASnakeActor()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;

}


void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	
}


void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeActor::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) 
	{
		FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase*NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	
}

void ASnakeActor::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	 MovementSpeed = ElementSize;

	switch(LastMoveDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X += MovementSpeed;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= MovementSpeed;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y -= MovementSpeed;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y += MovementSpeed;
			break;
	}

	//AddActorWorldOffset(MovementVector);

	for (int i = SnakeElements.Num() - 1; i > 0; i --)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	
SnakeElements[0]->AddActorWorldOffset(MovementVector);

}




